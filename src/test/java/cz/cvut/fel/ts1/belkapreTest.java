package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.fail;

public class belkapreTest {
    @Test
    public void factorialTest() {
        Belkapre belkapre = new Belkapre();
        int n = 2;
        long expectedResult = 2;

        long result = belkapre.factorial(n);

        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    public void factorialRecursiveTest() {
        Belkapre belkapre = new Belkapre();
        int n = 2;
        long expectedResult = 2;

        long result = belkapre.recursiveFactorial(n);

        Assertions.assertEquals(expectedResult, result);
    }
}
