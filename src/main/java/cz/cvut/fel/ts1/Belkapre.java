package cz.cvut.fel.ts1;

public class Belkapre {
    public long factorial (int n){
        long result = 1;
        int i = 1;
        while (i <= n){
            result = result * i;
            i++;
        }

        return result;
    }

    public long recursiveFactorial(int n) {
        if (n <= 1) return 1;
        return n * factorial(n - 1);
    }
}
